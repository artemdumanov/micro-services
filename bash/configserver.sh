#!/usr/bin/env bash
exec sudo apt-get update --yes
exec sudo apt-get upgrade --yes

# array of apt packages to install
declare -a packages=("")

for i in "${packages[@]}"
do
   exec sudo apt-get install --yes git nginx zlib1g-dev gcc make
done

# Python install
cd /tmp
exec wget https://www.python.org/ftp/python/3.6.1/Python-3.6.1.tar.xz
exec sudo tar -xvf Python-3.6.1.tar.xz -C /opt
cd /opt/Python-3.6.1
exec sudo ./configure --with-zlib=/usr/include --enable-optimization
exec sudo make
exec sudo make install
cd /srv