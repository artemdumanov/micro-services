#!/usr/bin/env bash

# ./usr/local/venv/bin/activate && cd /srv/micro-services && gunicorn webapps.scaler.scaler:app
# cd /usr/local/venv/ugram-services/bin/
# /bin/bash -c ". activate; exec /bin/bash -i"

export GOOGLE_APPLICATION_CREDENTIALS="ugram-04f86618b77c.json"
exec gunicorn -b :5000 -t 3600 webapps.scaler.scaler:app