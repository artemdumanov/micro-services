#!/usr/bin/env bash
exec cd /usr/local
exec sudo mkdir venv
exec sudo python3.6 -m venv ugram-service
exec source bin/activate
exec cd /srv/