from enum import Enum
from time import sleep

from utils import xlog
from .gcloud import GAPI

log = xlog.get_logger()


class InstanceType(Enum):
    F1_MICRO = 'f1-micro'
    G1_SMALL = 'g1-small'
    N1_STANDARD_1 = 'n1-standard-1'
    N1_STANDARD_2 = 'n1-standard-2'
    N1_STANDARD_4 = 'n1-standard-4'
    N1_STANDARD_8 = 'n1-standard-8'
    N1_STANDARD_16 = 'n1-standard-16'
    N1_STANDARD_32 = 'n1-standard-32'
    N1_STANDARD_64 = 'n1-standard-64'
    N1_HIGHMEM_2 = 'n1-highmem-2'
    N1_HIGHMEM_4 = 'n1-highmem42'
    N1_HIGHMEM_8 = 'n1-highmem-8'
    N1_HIGHMEM_16 = 'n1-highmem-16'
    N1_HIGHMEM_32 = 'n1-highmem-32'
    N1_HIGHMEM_64 = 'n1-highmem-64'
    N1_HIGHCPU_2 = 'n1-highcpu-2'
    N1_HIGHCPU_4 = 'n1-highcpu-4'
    N1_HIGHCPU_8 = 'n1-highcpu-8'
    N1_HIGHCPU_16 = 'n1-highcpu-16'
    N1_HIGHCPU_32 = 'n1-highcpu-32'
    N1_HIGHCPU_64 = 'n1-highcpu-64'


class InstanceStatus(Enum):
    RUNNING = 'RUNNING'
    TERMINATED = 'TERMINATED'
    DONE = 'DONE'


class ComputeEngineAPI(GAPI):
    def start_instance(self):
        try:
            request = self.service.instances().start(project=self.project,
                                                     zone=self.zone,
                                                     instance=self.instance)
            response = request.execute()
            self.wait_for_status(InstanceStatus.RUNNING)
        except Exception as e:
            log.critical(f'Project: {self.project}; '
                         f'Can\'t start an instance {self.instance} '
                         f'at {self.zone}', e)
        else:
            log.info(f'Project: {self.project}; '
                     f'Successfully start an instance {self.instance} '
                     f'at {self.zone}')

    def stop_instance(self):
        try:
            request = self.service.instances().stop(project=self.project,
                                                    zone=self.zone,
                                                    instance=self.instance)
            response = request.execute()
            self.wait_for_status(InstanceStatus.TERMINATED, delay=20)
        except Exception as e:
            log.critical(f'Project: {self.project}; '
                         f'Can\'t stop an instance {self.instance} '
                         f'at {self.zone}', e)
            raise
        else:
            log.info(f'Project: {self.project}; '
                     f'Successfully stop an instance {self.instance} '
                     f'at {self.zone}')

    def wait_for_status(self, status, delay=5):
        if not isinstance(status, InstanceStatus):
            raise ValueError("Use enum CoumpteEngineAPI.InstanceStatus "
                             "to specify waiting status")
        status = status.value

        log.info(f'Project: {self.project}; '
                 f'instance {self.instance}; '
                 f'Start waiting for status {status}')
        while True:
            instance_info = self.instance_info()
            if instance_info['status'] == status:
                log.info(f'Project: {self.project}; '
                         f'instance {self.instance}; '
                         f'Finish waiting for status {status}')
                sleep(delay)
                return

    def scale_sequence(self, scale_sequence):
        for index, instance in enumerate(scale_sequence):
            if not isinstance(instance, InstanceType):
                raise ValueError("Use enum CoumpteEngineAPI.InstanceType "
                                 "to specify scale sequence")
            scale_sequence[index] = instance.value

        if scale_sequence is None:
            raise ValueError("Use enum CoumpteEngineAPI.InstanceType "
                             "to specify scale sequence")

        self.scale_sequence = scale_sequence
        log.info(f'Project: {self.project}; '
                 f'Successfully set scale sequence: {scale_sequence}'
                 f'for instance {self.instance}')

    def scale_instance(self, next_type, machine_type=None):
        '''
        Scale an GCE instance, if next_type is True, else decrease.
        Or set machine type manually.
        :param next_type: scale up or decrease
        :param machine_type: or you can manually set machine type
        :return: None
        '''
        if self.scale_sequence is None:
            raise NotImplementedError('You must specify how to scale '
                                      'instances directly by scale_sequence()')
        try:
            machine_info = self.instance_info()
            *_, current_machine_type = machine_info["machineType"].rsplit("/")

            if next_type is not None:
                for key, instance_type in enumerate(self.scale_sequence):
                    if current_machine_type == instance_type:

                        machine_type = self.scale_sequence[key + 1 if next_type else key - 1 if key > 0 else key] \
                            if 0 <= key < len(self.scale_sequence) else current_machine_type

            log.info(f'Project: {self.project}; '
                     f'Successfully set machine type to scale: {machine_type}'
                     f'for instance {self.instance}')
        except Exception as e:
            log.exception(f'Project: {self.project}; '
                          f'Can\'t set machine type to scale: {machine_type}'
                          f'for instance {self.instance}. '
                          f'Check is instance type is too big, or too small', e)
            raise

        self.stop_instance()

        try:
            machine_type = 'zones/{}/machineTypes/{}' \
                .format(self.zone, machine_type)

            instances_set_machine_type_request_body = {
                'machineType': machine_type
            }

            request = self.service.instances() \
                .setMachineType(project=self.project,
                                zone=self.zone,
                                instance=self.instance,
                                body=instances_set_machine_type_request_body)

            request.execute()
            self.wait_for_status(InstanceStatus.TERMINATED)
        except Exception as e:
            log.critical(f'Project: {self.project}; '
                         f'Can\'t scale an instance {self.instance} '
                         f'to {machine_type} at {self.zone}', e)
            raise
        else:
            log.info(f'Project: {self.project}; '
                     f'Scaling and boot instance {self.instance} '
                     f'to {machine_type} at {self.zone}')

            self.start_instance()

            log.info(f'Project: {self.project}; '
                     f'Successfully scale and start instance {self.instance} '
                     f'to {machine_type} at {self.zone}')

    def instance_info(self):
        instance_list = self.service.instances().list(project=self.project,
                                                      zone=self.zone).execute()
        for instance in instance_list['items']:
            if instance['name'] == self.instance:
                log.info(f'Project: {self.project}; '
                         f'Get instance info of {self.instance};')
                return instance
