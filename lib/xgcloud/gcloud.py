from googleapiclient import discovery
from oauth2client.client import GoogleCredentials
from utils import xlog

log = xlog.get_logger()


class GcloudAPI:
    def __init__(self, project,
                 zone, instance):
        self.credentials = GoogleCredentials.get_application_default()
        self.service = discovery.build('compute', 'v1',
                                       credentials=self.credentials)
        # Project ID for this request.
        self.project = project
        # The name of the zone for this request.
        self.zone = zone
        # Name of the instance scoping this request.
        self.instance = instance

        log.info(f'Project: {project}; Successfully init gcloud API '
                 f'at zone: {zone} for instance: {instance}')

    def enable_api(self, API):
        API.enable(self.service, self.project, self.zone, self.instance)

        log.info(f'Project: {self.project}; Successfully '
                 f'enable API {API.__class__.__name__} for gclod')


class GAPI:
    def enable(self, service, project, zone, instance):
        self.service = service
        self.project = project
        self.zone = zone
        self.instance = instance
