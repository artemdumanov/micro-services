import logging
import os
from logging.config import dictConfig

LOG_ROOT = '/var/log/ugram-service'

logging_config = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)-6s %(levelname)-6s %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': logging.INFO,
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'info': {
            'class': 'logging.FileHandler',
            'formatter': 'standard',
            'level': logging.INFO,
            'filename': os.path.join(LOG_ROOT, 'info.log')
        },
        'error': {
            'class': 'logging.FileHandler',
            'formatter': 'standard',
            'level': logging.ERROR,
            'filename': os.path.join(LOG_ROOT, 'error.log')
        },
        'critical': {
            'class': 'logging.FileHandler',
            'formatter': 'standard',
            'level': logging.CRITICAL,
            'filename': os.path.join(LOG_ROOT, 'critical.log')
        }

    },
    'loggers': {
        '': {
            'handlers': ['default', 'info', 'error', 'critical'],
            'level': 'INFO',
            'propagate': True
        }
    }
}

dictConfig(logging_config)


def get_logger():
    return logging.getLogger()
