import sys
from setuptools import setup

setup(
    name='scaler',
    packages=['scaler'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)

