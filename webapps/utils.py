from flask import request, Response

from utils import xlog

log = xlog.get_logger()

__token = 'HsdfHjm_OJJNBjknkmKMs_K_NPIdsdLXdKdfdfIUYHuIOL'


def check_token(auth_token):
    return __token == auth_token


def is_token_valid(fn, msg):
    log.info(f'Check token of {msg} action')
    auth_token = request.args.get('auth_token')

    if auth_token is not None and check_token(auth_token):
        try:
            fn()
        except:
            return Response('Something was wrong')

        return Response(msg)
    else:
        return Response("", 403)
