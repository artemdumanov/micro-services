from functools import partial

from flask import Flask, request, Response

from lib.xgcloud.compute import ComputeEngineAPI, InstanceType
from lib.xgcloud.gcloud import GcloudAPI
from webapps.utils import check_token, is_token_valid

app = Flask(__name__)

ugram_gcloud = GcloudAPI(project='ugrambeta',
                         zone='europe-west1-d',
                         instance='ugram-ua-1')

ugram_compute = ComputeEngineAPI()

ugram_gcloud.enable_api(ugram_compute)

ugram_compute.scale_sequence(
    scale_sequence=[
        InstanceType.F1_MICRO,
        InstanceType.G1_SMALL,
        InstanceType.N1_STANDARD_1,
        InstanceType.N1_STANDARD_2,
        InstanceType.N1_HIGHMEM_2,
        InstanceType.N1_HIGHMEM_4,
        InstanceType.N1_HIGHMEM_8,
        InstanceType.N1_HIGHMEM_16,
        InstanceType.N1_HIGHMEM_32,
        InstanceType.N1_HIGHMEM_64

    ]
)


@app.route('/start', methods=['GET', 'POST'])
def start():
    return is_token_valid(ugram_compute.start_instance, 'start..')


@app.route('/stop', methods=['GET', 'POST'])
def stop():
    return is_token_valid(ugram_compute.stop_instance, 'stop..')


@app.route('/scale', methods=['GET', 'POST'])
def scale():
    return is_token_valid(partial(ugram_compute.scale_instance, True), 'scale..')


@app.route('/decrease', methods=['GET', 'POST'])
def decrease():
    return is_token_valid(partial(ugram_compute.scale_instance, False), 'decrease..')
